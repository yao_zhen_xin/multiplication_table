﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 徐汉权
{
    class Program
    {
        static void Main(string[] args)
        {
            //九九乘法表。
            var Str = "";
            for(int i = 1; i <=9; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    Str +=" " + j + "x" + i + "=" + i*j +"\t";
                }
                Str += " \n";
            }
            Console.WriteLine(Str);
        }
    }
}
